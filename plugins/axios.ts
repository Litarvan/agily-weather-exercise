import axios from 'axios';

import { defineNuxtPlugin } from '#app';

export default defineNuxtPlugin(() => {
    return {
        provide: {
            axios: () => {
                return axios.create({
                    baseURL: 'http://localhost:4000/'
                })
            }
        }
    }
});
