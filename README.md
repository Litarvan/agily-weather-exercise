# The Forecast Weather App

## Mise en route

### Back-end

```
cd backend
npm i
node index.mjs
```

### Front-end

```
npm i
npm run dev
```

## Exercice 1 (dossier actuel)

### Choix techniques

- Nuxt 3 : J'ai fait le choix d'utiliser Nuxt, car
  je suis un peu plus fan de Vue.JS que de React, et parce que le SSR c'est plutôt sympa. J'ai aussi pris la liberté
  d'utiliser la version 3 de Nuxt, actuellement en Release Candidate, car ça m'a permis d'utiliser la syntaxe de
  composition de Vue 3 qui ressemble beaucoup à Svelte, mon framework préféré.
- SASS : J'utilise toujours SASS, car j'ai besoin notamment des variables, des mixins, du nesting et des imports.
- NormalizeCSS : J'utilise toujours NormalizeCSS pour avoir une base de CSS un plus propre.
- Axios : Imposé par la consigne mais évidemment très utile pour les requêtes que j'aurai à faire au backend.
- TypeScript : J'ai choisi le TypeScript pour le front-end, car le typage c'est parfois plutôt sympa pour éviter
  quelques erreurs et c'est bien supporté par Nuxt 3.

Pour le CSS, je nest toujours mes éléments, car ça créé une sorte d'arbre qui rend je trouve beaucoup plus simple la
navigation dans le CSS. Je range aussi toujours mes règles par catégorie, dans le même ordre.

Pour mes imports JS/TS, je les range toujours aussi par catégorie dans le même ordre.

### Déroulement

J'ai commencé par réaliser quelques assets, notamment le logo et l'icône de recherche. J'ai fait tout ça sur Sketch
en vectoriel, car c'est un logiciel que j'ai l'habitude d'utiliser et que je n'avais pas besoin d'Illustrator pour des
images aussi simples. Et évidemment toujours faire du vectoriel quand c'est possible, pour supporter toutes les
résolutions et avoir des fichiers bien plus légers et surtout manipulables via du CSS. J'ai aussi fait une petite icône,
car c'est toujours sympa.

J'ai ensuite initialisé le projet, rajouté un peu de SCSS de base, mis en place Axios, et commencé à réaliser les deux
pages.

En tout, le front-end m'a pris environ deux/trois heures à faire.

La consigne indiquait de ne pas utiliser de service externe sans que ça soit demandé, mais je ne voyais pas comment
utiliser une image de fond dépendante de la ville sans un service dédié, je me suis donc permis d'utiliser Flickr
plutôt que de ne pas implémenter la fonctionnalité (ou de le faire sur un set précis de ville), ce qui aurait été
dommage.

### Fonctionnalités

La page de recherche (/) permet de rechercher une ville, il faut pour ça taper un nom complet (imposé par l'endpoint 
d'OpenWeatherAPI demandé), il proposera ensuite au bout d'1 seconde sans input une liste de villes qui correspondent.
On peut sélectionner la ville avec les flèches du clavier et valider avec la touche entrée, ou tout simplement cliquer
dessus.

La page principale (/weather) affiche la météo d'aujourd'hui et des 7 prochains jours, on peut cliquer sur un des jours
pour voir les détails, et cliquer à nouveau dessus pour revenir à la météo d'aujourd'hui. Le fond change en fonction
de la ville via Flickr (ce qui peut entraîner parfois des images un peu décalées, mais toujours très jolies).

Les requêtes vers le backend utilisent les données asynchrones Nuxt, elles sont donc fetch par le client lors
d'un changement de page dynamique (depuis la page de recherche par exemple), et par le serveur lorsqu'on accède à la
page via son URL directement.

Évidemment, les deux pages sont responsive.

### Pistes d'amélioration

- Le design pourrait être amélioré
- Les erreurs pourraient être mieux gérées
- Plus d'animations pourraient être rajoutées
- Un chargement entre les deux pages pourrait être ajouté
- Ajouter le support pour les autres langues
- Documenter et commenter le code
- Utiliser ESLint et Prettier
- Ajouter des tests et des outils de coverage
- Mettre en place une CI pour lancer les tests et vérifier la coverage
 
## Exercice 2 (dossier `backend`)

### Choix techniques

- **Express** : Imposé par la consigne, j'aurais pu utiliser Polka qui est une implémentation bien plus rapide de la même
  API, mais bon ici on s'en fiche un peu.
- **Node-Cache** : Imposé par la consigne, un peu plus simple qu'un Redis c'est sûr.
- **Fast-XML-Parser** : Pour parser la sortie de l'API Flickr, j'ai utilisé Fast-XML-Parser qui est très rapide et
  écrit entièrement en JavaScript.
- **Axios** : Imposé par la consigne, toujours sympa.
- **JavaScript** : J'ai pris JavaScript plutôt que TypeScript, car ça me permet de ne pas avoir d'étape de transpilation,
  ici c'est quand même plus simple même si du TypeScript avec les types du front-end partagés aurait été la bienvenue.
- **JSON** : Je communique avec le front-end en JSON, parce qu'en JS c'est vachement simple.

J'ai utilisé le système de module plutôt que les require CommonJS classiques, car plus moderne et plus puissant.

### Déroulement

Après avoir terminé le front-end, j'ai rapidement mis en place Express (avec du CORS), Axios et Node-Cache, puis j'ai
rapidement créé mes petites routes.

En tout le back-end m'a pris moins d'une heure à faire.

### Fonctionnalités

Le backend utilise l'API d'OpenWeatherMap pour récupérer les infos sur les prévisions météo, et l'API FlickR pour
récupérer les images de fond en fonction des villes. Tout est mis en cache pour une durée de 15 minutes (parce que quand
même il faut que ça se mette à jour régulièrement).

Pour l'image de fond, je cherche en priorité des photos d'au moins 1920x1080 (même si du 3140x2160 serait préférable),
qui sont prises à l'extérieur.

### Pistes d'amélioration

- Meilleure gestion d'erreur
- Utiliser Polka plutôt qu'Express, voire un framework plus poussé (Nest.JS, Adonis.JS)
- Utiliser du TypeScript et partager les types avec le front-end
- Utiliser Redis plutôt que Node-Cache (pour pouvoir scale sans soucis)
- Utiliser Dotenv plutôt que de mettre les secrets dans le code
- Redimensionner l'image de fond, car actuellement elles sont parfois tellement grosses qu'elles ruinent l'animation 
  d'apparition du front-end.
- Documenter et commenter le code
- Utiliser ESLint et Prettier
- Ajouter des tests et des outils de coverage
- Mettre en place une CI pour lancer les tests et vérifier la coverage
