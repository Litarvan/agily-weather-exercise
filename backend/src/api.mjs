import axios from 'axios';

// In a real app we would have use `dotenv` of course
const OWM_API_KEY = 'db988691faf182dfc3750cd1e57f3718';
const FLICKR_API_KEY = 'e8a35928217d50c029e5f61b8ef57da2';
const FLICKR_API_SECRET = '266286fc924d4ba2';

const owm = axios.create({ baseURL: 'https://api.openweathermap.org/' });
const flickr = axios.create({ baseURL: 'https://api.flickr.com/' });

export async function callOWM(route, params)
{
    return owm.get(route, {
        params: {
            ...params,
            appid: OWM_API_KEY
        }
    }).then(r => r.data);
}

export async function callFlickr(method, params)
{
    return flickr.get('/services/rest/', {
        params: {
            ...params,
            method,
            api_key: FLICKR_API_KEY
        }
    }).then(r => r.data);
}
