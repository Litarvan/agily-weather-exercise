import { XMLParser } from 'fast-xml-parser';

import { callFlickr } from './api.mjs';
import { has, get, update } from './cache.mjs';

// In real life, this would be in a .env file
const API_KEY = 'e8a35928217d50c029e5f61b8ef57da2';
const API_SECRET = '266286fc924d4ba2';

export async function getBackground(lat, lon)
{
    if (has('background', [lat, lon])) {
        return get('background', [lat, lon]);
    }

    const data = await callFlickr('flickr.photos.search', {
        lat, lon,
        media: 'photos',
        extras: 'url_o,o_dims',
        geo_context: 2,
    });
    const parser = new XMLParser({
        ignoreAttributes: false
    });

    const photos = parser.parse(data).rsp.photos.photo;
    let photo, tries = 0;
    do {
        photo = photos[Math.floor(Math.random() * photos.length)];
        tries++;
    } while (tries < 50 && (photo['@_width_o'] < 1920 || photo['@_height_o'] < 1080 || !photo['@_url_o']));

    const picked = photo || photos[0];
    const url = picked['@_url_o'] || createURL(picked['@_server'], picked['@_id'], picked['@_secret']);

    return update('background', [lat, lon], url);
}

function createURL(server, id, secret, size = 'l')
{
    return `https://live.staticflickr.com/${server}/${id}_${secret}_${size}.jpg`;
}
