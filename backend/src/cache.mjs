import NodeCache from 'node-cache';

const cache = new NodeCache({ stdTTL: 15 * 60 }); // 15 Minutes cache

export function has(group, keys)
{
    return cache.has(keyFor(group, keys));
}

export function get(group, keys)
{
    return cache.get(keyFor(group, keys));
}

export function update(group, keys, data)
{
    cache.set(keyFor(group, keys), data);
    return data;
}

function keyFor(group, keys)
{
    return `${group}=${keys.join(',')}`;
}
