import { callOWM } from './api.mjs';
import { get, has, update } from './cache.mjs';

export async function getCities(query)
{
    if (has('cities', [query])) {
        return get('cities', [query]);
    }

    const cities = await callOWM('/geo/1.0/direct', {
        q: query,
        limit: 5
    });

    return update('cities', [query], cities.map(({ name, lat, lon, country, state }) => ({
        name, lat, lon, country, state
    })));
}

export async function getWeather(lat, lon)
{
    if (has('weather', [lat, lon])) {
        return get('weather', [lat, lon]);
    }

    const { daily } = await callOWM('/data/2.5/onecall', {
        lat, lon,
        exclude: 'hourly,minutely',
        units: 'metric'
    });

    const [today, ...week] = daily.map(d => ({
        date: d.dt,
        icon: getIcon(d.weather[0].icon),
        dayTemperature: d.temp.day,
        nightTemperature: d.temp.night,
        humidity: d.humidity,
        pressure: d.pressure,
        wind: d.wind_speed * 60 * 60 / 1000 // m/s -> km/h
    }));

    return update('weather', [lat, lon], { today, week });
}

function getIcon(id)
{
    return `https://openweathermap.org/img/wn/${id}@4x.png`;
}
