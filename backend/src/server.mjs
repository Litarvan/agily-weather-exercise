import express from 'express';
import cors from 'cors';

import { getCities, getWeather } from './weather.mjs';
import { getBackground } from './background.mjs';

const PORT = 4000;
const app = express();

// We should use proper CORS instead
app.use(cors());

app.get('/cities', (req, res) => {
    const { query } = req.query;
    if (!query) {
        return error(res, new Error('Missing query parameter'));
    }

    getCities(query)
        .then(data => res.json(data))
        .catch(err => error(res, err));
})

app.get('/weather', (req, res) => {
    const { lat, lon } = req.query;
    if (!lat || !lon) {
        return error(res, new Error('Missing lat or lon query parameter'));
    }

    getWeather(lat, lon)
        .then(data => res.json(data))
        .catch(err => error(res, err));
});

app.get('/background', (req, res) => {
    const { lat, lon } = req.query;
    if (!lat || !lon) {
        return error(res, new Error('Missing lat or lon query parameter'));
    }

    getBackground(lat, lon)
        .then(data => res.json(data))
        .catch(err => error(res, err));
});

export function start()
{
    app.listen(PORT, () => {
        console.log(`Listening on port ${PORT}...`)
    });
}

function error(res, error)
{
    console.error('Error during request');
    console.error(error);

    res.json({
        success: false,
        error: error.message
    });
}
