import { defineNuxtConfig } from 'nuxt';

export default defineNuxtConfig({
    app: {
        head: {
            title: 'The Forecast Weather App',
            meta: [
                { name: 'viewport', content: 'width=device-width, initial-scale=1' }
            ],
            link: [
                { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
            ]
        }
    },
    css: [
        '@/assets/styles/app.scss'
    ]
});
