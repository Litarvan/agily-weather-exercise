export interface WeatherData
{
    today: DayWeatherData,
    week: DayWeatherData[]
}

export interface DayWeatherData
{
    date: number,
    icon: string,
    dayTemperature: number,
    nightTemperature: number,
    humidity: number,
    pressure: number,
    wind: number
}
