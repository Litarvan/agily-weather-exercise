export interface City
{
    name: string,
    country: string,
    state?: string,

    lat: number,
    lon: number
}

export function formatCity(city: City): string
{
    return [city.name, city.state, city.country]
        .filter(s => s)
        .map(s => s.replaceAll('-', '‒'))
        .join(', ');
}
